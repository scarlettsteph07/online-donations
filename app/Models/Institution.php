<?php

namespace Donations\Models;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    public function country() {
    	return $this->belongsTo('Donations\Models\Country');
  	}

  	public function donations(){
    	return $this->hasMany('Donations\Models\Donation');
  	}
}
