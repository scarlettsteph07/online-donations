<?php

namespace Donations\Models;

use Illuminate\Database\Eloquent\Model;

class CreditCardBrand extends Model
{
	protected $table = 'brands';

    public function creditCards(){
    	return $this->hasMany('Donations\Models\CreditCard');
  	}
}
