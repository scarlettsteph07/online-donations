<?php

namespace Donations\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function users(){
    	return $this->hasMany('Donations\Models\User');
  	}

  	public function institutions(){
    	return $this->hasMany('Donations\Models\Intitutions');
  	}
}
