<?php

namespace Donations\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public function country() {
    	return $this->belongsTo('Donations\Models\Country');
  	}

  	public function role() {
    	return $this->belongsTo('Donations\Models\Role');
  	}

  	public function donations(){
    	return $this->hasMany('Donations\Models\Donation');
  	}

  	public function creditCards(){
    	return $this->hasMany('Donations\Models\CreditCard');
  	}
}
