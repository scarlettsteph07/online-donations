<?php

namespace Donations\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Donation extends Model
{
    public function institution() {
    	return $this->belongsTo('Donations\Models\Institution');
  	}

  	public function user() {
    	return $this->belongsTo('Donations\Models\User');
  	}

  	public function creditCard() {
    	return $this->belongsTo('Donations\Models\CreditCard');
  	}

  	public static function avoid_duplicates() {
  		// Actual query:
  		// SELECT month, country, COUNT(*) as count FROM ( 
  		//  SELECT MONTH(d.date) as month, c.name as country
		// 	FROM `donations` AS d
		// 	JOIN institutions as i
		// 	ON d.institution_id = i.id
		// 	JOIN countries as c
		// 	ON i.country_id = c.id
		// 	WHERE user_id = 3
		    
		// ) as country_month GROUP BY country

  		$result = DB::table('donations')
                     ->select(DB::raw('count(*) as count'))
                     ->join('institutions', 'donations.institution_id', '=', 'institutions.id')
                     ->join('countries', 'institutions.country_id', '=', 'countries.id')
                     ->where('user_id', '=', 3)
                     ->groupBy('countries.id')
                     ->get();
		return $result;                     
  	}
}
