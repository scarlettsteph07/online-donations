<?php

namespace Donations\Models;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
    public function user() {
    	return $this->belongsTo('Donations\Models\User');
  	}

  	public function donations(){
    	return $this->hasMany('Donations\Models\Donation');
  	}

  	public function creditCardBrand() {
    	return $this->belongsTo('Donations\Models\CreditCardBrand');
  	}

}
