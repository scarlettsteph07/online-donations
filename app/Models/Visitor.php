<?php

namespace Donations\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Visitor extends Model
{
    public $attributes = [ 'hits' => 0 ];

    protected $fillable = [ 'ip', 'visit_date' ];
    protected $table = 'visitors';

    public static function boot() {
        // Any time the instance is updated (but not created)
        static::saving( function ($visitor) {
            $visitor->visit_time = date('H:i:s');
            $visitor->hits++;
        } );
    }

    public static function hit() {
        // static::firstOrCreate([
        	static::create([
                  'ip'   => $_SERVER['REMOTE_ADDR'],
                  'visit_date' => date('Y-m-d'),
              ])->save();
    }

    public static function per_day() {
        $visitors = DB::table('visitors')
            ->select('visit_date', DB::raw('count(*) as total'))
            ->groupBy('visit_date')
            ->paginate(10);
        return $visitors;
    }


}
