<?php

namespace Donations\Http\Controllers\Admin;
use Validator;

use Illuminate\Http\Request;
use Donations\Models\Country;

use Donations\Http\Requests;
use Donations\Http\Controllers\Controller;

class CountryController extends Controller
{
    public function index() {
        $countries = Country::paginate(10);
        return view('admin.countries.index', ['countries' => $countries]);
    }

    public function create()
    {
        return view('admin.countries.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:25',
        ]);

        if ($validator->fails()) {
            return redirect('admin/countries/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $country = new Country;

        // insert into countries table
        $country->name = $request->name;
        $country->save();

        return redirect('/admin/countries'); //once stored, redirect to index
    }

    public function show($id)
    {   
        $country = Country::find($id);
        return view('admin.countries.show', ['country' => $country]);
    }

    public function edit($id)
    {
        $country = Country::find($id);
        return view('admin.countries.edit', ['country' => $country]);
    }

    public function update(Request $request, $id)
    {
         $validator = Validator::make($request->all(), [
            'name' => 'required|max:25',
        ]);

        if ($validator->fails()) {
            return redirect('admin/countries/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $country = Country::find($id);

        // update countries table
        $country->name = $request->name;
        $country->save();

        return redirect('/admin/countries'); //once updated, redirect to index
    }

    public function destroy($id)
    {
        $country = Country::find($id);
        $country->delete();
        return redirect('/admin/countries'); //once deleted, redirect to index
    }
}
