<?php

namespace Donations\Http\Controllers\Admin;
use Validator;
use DB;

use Illuminate\Http\Request;
use Donations\Models\Institution;
use Donations\Models\Country;


use Donations\Http\Requests;
use Donations\Http\Controllers\Controller;

class InstitutionController extends Controller
{
    public function index() {

        $institutions = Institution::paginate(10);
        return view('admin.institutions.index', ['institutions' => $institutions]);
    }

    public function create()
    {
        $countries = Country::all();
        return view('admin.institutions.create', ['countries' => $countries]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:25',
            'description' => 'required|max:1500',
            'department' => 'required|max:25',
            'country' => 'required|max:50',
        ]);

        if ($validator->fails()) {
            return redirect('admin/institutions/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $institutions = new Institution;

        // insert into institutions table
        $institutions->name = $request->name;
        $institutions->description = $request->description;
        $institutions->department = $request->department;
        $institutions->country_id = $request->country;
        $institutions->save();

        return redirect('/admin/institutions'); //once stored, redirect to index
    }

    public function show($id)
    {   
        $institution = Institution::find($id);
        return view('admin.institutions.show', ['institution' => $institution]);
    }

    public function edit($id)
    {
        $institution = Institution::find($id);
        $countries = Country::all();
        return view('admin.institutions.edit', ['institution' => $institution, 'countries' => $countries]);
    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:25',
            'description' => 'required|max:1500',
            'department' => 'required|max:25',
            'country' => 'required|max:50',
        ]);

        if ($validator->fails()) {
            return redirect('admin/institutions/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }

        $institution = Institution::find($id);

        // update institutions table
        $institution->name = $request->name;
        $institution->description = $request->description;
        $institution->department = $request->department;
        $institution->country_id = $request->country;
        $institution->save();

        return redirect('/admin/institutions'); //once updated, redirect to index
    }

    public function destroy($id)
    {
        $institution = Institution::find($id);
        $institution->delete();
        return redirect('/admin/institutions'); //once deleted, redirect to index
    }
}
