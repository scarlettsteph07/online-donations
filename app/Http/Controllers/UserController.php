<?php

namespace Donations\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

use Donations\Http\Requests;
use Donations\Http\Controllers\Controller;

use Donations\Models\User;


class UserController extends Controller
{
    public function index() 
    {
        $users = User::paginate(10);
        // $users = User::all();
        return view('users.index', ['users' => $users]);
    }

    public function show($id) 
    {
        $user = User::find($id);
        return view('users.show', ['user' => $user]);
    }

    public function edit($id) 
    {   
    	$user = User::find($id);
    	return view('users.edit', ['user' => $user]);
    }

    public function update(Request $request, $id) 
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:25',
            'surname' => 'required|max:25',
            'document' => 'required|max:10',
            'email' => 'required|email|max:50',
        ]);

        if ($validator->fails()) {
            return redirect('users/'. $id .'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }


    	$user = User::find($id);
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->document = $request->document;
        $user->email = $request->email;
        // $user->role_id = 2;
        $user->save();
    	// return view('users.show', ['user' => $user]);
    	return redirect('users/'.$id);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('/'); //once deleted, redirect to index
    }
}
