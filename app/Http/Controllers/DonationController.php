<?php

namespace Donations\Http\Controllers;
use Validator;

use Illuminate\Http\Request;
use Donations\Models\User;
use Donations\Models\Donation;
use Donations\Models\Institution;
use Donations\Models\CreditCard;

use Donations\Http\Requests;
use Donations\Http\Controllers\Controller;

class DonationController extends Controller
{
    public function showAll()
    {
        $donations = Donation::paginate(10);
        return view('donations.all', ['donations' => $donations]);
    }

	public function index($user_id)
    {
    	$user = User::find($user_id);
        $donations = Donation::where('user_id', '=', $user_id)->paginate(10);
        return view('donations.index', ['donations' => $donations, 'user' => $user]);
    }

    public function create($user_id) {

    	$user = User::find($user_id);
        $institutions = Institution::all();
        $cards = CreditCard::where('user_id', '=', $user_id)->get();
        return view('donations.create', ['institutions' => $institutions, 'user' => $user, 'cards' => $cards]);
    }

    public function store(Request $request, $user_id)
    {   
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric|max:1000',
            'institution' => 'required',
            'card' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('users/'.$user_id.'/donations/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $donation = new Donation;

        // insert into donations table
        $donation->amount = $request->amount;
        $donation->date = date('Y-m-d');
        $donation->institution_id = $request->institution;
        $donation->user_id = $user_id;
        $donation->credit_card_id = $request->card;
        $donation->save();

        return redirect('users/'.$user_id.'/donations'); //once stored, redirect to index
    }

    public function show($user_id, $donation_id) 
    {
        $user = User::find($user_id);
        $donation = Donation::where('id', '=', $donation_id)->where('user_id', '=', $user_id)->first();
        return view('donations.show', ['user' => $user, 'donation' => $donation]);
    }

    public function results() {
        $result = Donation::avoid_duplicates();
        return view('donations.results', ['result'=>$result]);
    }

}
