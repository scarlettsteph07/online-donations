<?php

namespace Donations\Http\Controllers;
use Validator;

use Illuminate\Http\Request;
use Donations\Models\User;
use Donations\Models\CreditCard;
use Donations\Models\CreditCardBrand;

use Donations\Http\Requests;
use Donations\Http\Controllers\Controller;

class CreditCardController extends Controller
{
    public function index($user_id)
    {   
        $user = User::find($user_id);
        $cards = CreditCard::where('user_id', '=', $user_id)->paginate(10);
        return view('creditcards.index', ['cards' => $cards, 'user' => $user]);
    }

    public function create($id)
    {
        $user = User::find($id);
        $brands = CreditCardBrand::all();
        return view('creditcards.create', ['brands' => $brands, 'user' => $user]);
    }

    public function store(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:25',
            'number' => 'required|numeric',
            'last_four' => 'required|numeric|digits:4',
            'expiration' => 'required|numeric|digits:4',
            'cvv' => 'required|numeric|digits:3',
            'brand' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('users/'.$id.'/cards/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $card = new Creditcard;

        // insert into creditcards table
        $card->name = $request->name;
        $card->number = $request->number;
        $card->last_four = $request->last_four;
        $card->expiration = $request->expiration;
        $card->cvv = $request->cvv;
        $card->brand_id = $request->brand;
        $card->user_id = $id;
        $card->save();

        return redirect('users/'.$id.'/cards'); //once stored, redirect to index
    }

    public function edit($user_id, $card_id)
    {   
        $user = User::find($user_id);
        $card = CreditCard::where('id', '=', $card_id)->where('user_id', '=', $user_id)->first();
        $brands = CreditCardBrand::all();
        return view('creditcards.edit', ['user' => $user, 'card' => $card, 'brands' => $brands]);
    }

    public function update(Request $request, $user_id, $card_id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:25',
            'expiration' => 'required|numeric|digits:4',
            'cvv' => 'required|numeric|digits:3',
        ]);

        if ($validator->fails()) {
            return redirect('users/'.$user_id.'/cards/'.$card_id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = User::find($user_id);
        $card = CreditCard::where('id', '=', $card_id)->where('user_id', '=', $user_id)->first();

        $card->name = $request->name;
        $card->expiration = $request->expiration;
        $card->cvv = $request->cvv;
        $card->save();

        return redirect('users/'.$user_id.'/cards'); //once updated, redirect to index
    }

    public function destroy($user_id, $card_id)
    {
        $user = User::find($user_id);
        $card = CreditCard::where('id', '=', $card_id)->where('user_id', '=', $user_id)->first();
        $card->delete();
        return redirect('users/'.$user_id.'/cards'); //once updated, redirect to index
    }

}
