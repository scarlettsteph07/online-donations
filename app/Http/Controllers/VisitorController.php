<?php

namespace Donations\Http\Controllers;

use Illuminate\Http\Request;

use Donations\Http\Requests;
use Donations\Http\Controllers\Controller;
use Donations\Models\Visitor;
use Donations\Models\User;
use DB;
use Storage;

class VisitorController extends Controller
{
    public function welcome()
    {
        $count = Visitor::all()->count();
        Visitor::hit();
        return view('welcome', ['count' => $count]);
    }

    // Show visitors per day
    public function index()
    {
        // $visitors = Visitor::all(); //where('visit_date', date('Y-m-d'));

        
        // $visitors = DB::table('visitors')
        //          ->select('visit_date', DB::raw('count(*) as total'))
        //          ->groupBy('visit_date')
        //          ->get();

        $visitors = Visitor::per_day();
        $count =  10;//Visitor::where('visit_date', date('Y-m-d'))->count();
        return view('visitors.index', ['count' => $count, 'visitors' => $visitors ]);
    }

    public function save_txt()
    {	
    	$visitors = Visitor::per_day();

    	$content = 'Date 	   | # Visits';
    	foreach($visitors as $visitor) {
   			$content .=  "\n" . $visitor->visit_date . ' | ' . $visitor->total;
		}

    	// $content = print_r($visitors, true); // this one works!
    	Storage::disk('local')->put('visitors.txt', $content);
        $pathToFile = storage_path(). "/app/visitors.txt";
        return response()->download($pathToFile, 'visitors.txt');
    }


}
