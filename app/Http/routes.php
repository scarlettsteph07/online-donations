<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/results', 'DonationController@results');
Route::get('/', 'VisitorController@welcome');
// Route::get('/home', view('welcome'));
// Route::get('/home', function () {
//     return view('welcome');
// });

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', function()
    {
        if (Auth::guest()) {
            return redirect('welcome');
        }
        elseif (Auth::user()->role_id==1){
            return redirect('admin/institutions');
        }
        elseif (Auth::user()->role_id==2){
            return redirect('users/' . Auth::user()->id . '/donations');
        }
    });

    Route::get('/home', function()
    {
        if (Auth::guest()) {
            return redirect('welcome');
        }
        elseif(Auth::user()->role_id==1){
            return redirect('admin/institutions');
        }
        elseif (Auth::user()->role_id==2){
            return redirect('users/' . Auth::user()->id . '/donations');
        }

    });

    Route::get('/welcome', 'VisitorController@welcome');
    Route::get('/visitors', 'VisitorController@index');
    Route::get('/visitors/print', 'VisitorController@save_txt');

    // Route::get('/home', 'HomeController@index');
    Route::get('/users/{id}', 'UserController@show');
    Route::get('/users/{id}/edit', 'UserController@edit');
    Route::post('/users/{id}/update', 'UserController@update');
    Route::get('users/{id}/destroy', 'UserController@destroy');

    Route::get('/users/{id}/cards', 'CreditCardController@index');
    Route::get('/users/{id}/cards/create', 'CreditCardController@create');
    Route::post('/users/{id}/cards', 'CreditCardController@store');
    Route::get('/users/{id}/cards/{ccid}/edit', 'CreditCardController@edit');
    Route::post('/users/{id}/cards/{ccid}/update', 'CreditCardController@update');

    Route::get('/users/{id}/cards/{ccid}/destroy', 'CreditCardController@destroy');

    Route::get('/users/{id}/donations', 'DonationController@index');
    Route::get('/users/{id}/donations/create', 'DonationController@create');
    Route::post('/users/{id}/donations', 'DonationController@store');
    Route::get('/users/{id}/donations/{donation_id}', 'DonationController@show');

    Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function()
    {
        // Controllers Within The "App\Http\Controllers\Admin" Namespace
        Route::get('/users', '\Donations\Http\Controllers\UserController@index');
        Route::get('/donations', '\Donations\Http\Controllers\DonationController@showAll');

        Route::get('/institutions', 'InstitutionController@index');
        Route::get('/institutions/create', 'InstitutionController@create');
        Route::post('/institutions', 'InstitutionController@store');
        Route::get('/institutions/{id}', 'InstitutionController@show');
        Route::get('/institutions/{id}/edit', 'InstitutionController@edit');
        Route::post('/institutions/{id}/update', 'InstitutionController@update');
        Route::get('/institutions/{id}/destroy', 'InstitutionController@destroy');

        Route::get('/countries', 'CountryController@index');
        Route::get('/countries/create', 'CountryController@create');
        Route::post('/countries', 'CountryController@store');
        Route::get('/countries/{id}', 'CountryController@show');
        Route::get('/countries/{id}/edit', 'CountryController@edit');
        Route::post('/countries/{id}/update', 'CountryController@update');
        Route::get('/countries/{id}/destroy', 'CountryController@destroy');

    });

});
