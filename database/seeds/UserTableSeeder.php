<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
      	  [
      	  'id' => 1, 
      	  'name' => "Admin", 
          'surname' => "Administrator", 
          'document' => "adm1n1strat0r", 
      	  'email' => "admin@donations.com", 
      	  'password' => bcrypt('secret'),
          'role_id' => 1],
	    ]);
    }
}
