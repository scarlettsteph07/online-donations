<?php

use Illuminate\Database\Seeder;

class RolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
      	  [
      	  'id' => 1, 
      	  'name' => "Admin", 
      	  'description' => "System Administrator" ],
	    ]);

	    DB::table('roles')->insert([
      	  [
      	  'id' => 2, 
      	  'name' => "User", 
      	  'description' => "Any person who has register with an email address"],
	    ]);
    }
}
