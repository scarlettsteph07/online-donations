<?php

use Illuminate\Database\Seeder;

class InstitutionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('institutions')->insert([
      	  [
      	  'id' => 1, 
      	  'name' => "Institution 1", 
      	  'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eleifend vehicula sodales. Nunc at sapien quam. Cras eu augue sit amet nulla fermentum iaculis auctor sed magna. Quisque in urna sem. Nam cursus felis magna, vel tempor lectus ullamcorper congue. Integer mattis gravida dui eu maximus. Proin viverra mollis rutrum. Donec ut nisi nec erat vehicula maximus. Mauris eu nisl orci. Donec ac semper turpis, non egestas mi. Aenean volutpat, est quis tristique blandit, metus ipsum sodales turpis, ac lacinia velit nisi eu mauris. Vestibulum nec condimentum augue. Vestibulum nec ornare mauris, sed iaculis purus. Vivamus eu semper odio.',
      	  'department' => 'San Salvador',
      	  'country_id' => 1],
	    ]);

	    // DB::table('institutions')->insert([
     //  	  [
     //  	  'id' => 2, 
     //  	  'name' => "Charity 2", 
     //  	  'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eleifend vehicula sodales. Nunc at sapien quam. Cras eu augue sit amet nulla fermentum iaculis auctor sed magna. Quisque in urna sem. Nam cursus felis magna, vel tempor lectus ullamcorper congue. Integer mattis gravida dui eu maximus. Proin viverra mollis rutrum. Donec ut nisi nec erat vehicula maximus. Mauris eu nisl orci. Donec ac semper turpis, non egestas mi. Aenean volutpat, est quis tristique blandit, metus ipsum sodales turpis, ac lacinia velit nisi eu mauris. Vestibulum nec condimentum augue. Vestibulum nec ornare mauris, sed iaculis purus. Vivamus eu semper odio.',
     //  	  'department' => 'Santa Ana',
     //  	  'country_id' => 1],
	    // ]);

	    // DB::table('institutions')->insert([
     //  	  [
     //  	  'id' => 3, 
     //  	  'name' => "Charity 3", 
     //  	  'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eleifend vehicula sodales. Nunc at sapien quam. Cras eu augue sit amet nulla fermentum iaculis auctor sed magna. Quisque in urna sem. Nam cursus felis magna, vel tempor lectus ullamcorper congue. Integer mattis gravida dui eu maximus. Proin viverra mollis rutrum. Donec ut nisi nec erat vehicula maximus. Mauris eu nisl orci. Donec ac semper turpis, non egestas mi. Aenean volutpat, est quis tristique blandit, metus ipsum sodales turpis, ac lacinia velit nisi eu mauris. Vestibulum nec condimentum augue. Vestibulum nec ornare mauris, sed iaculis purus. Vivamus eu semper odio.',
     //  	  'department' => 'Ahuachapan',
     //  	  'country_id' => 1],
	    // ]);

	    // DB::table('institutions')->insert([
     //  	  [
     //  	  'id' => 4, 
     //  	  'name' => "Charity 4", 
     //  	  'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eleifend vehicula sodales. Nunc at sapien quam. Cras eu augue sit amet nulla fermentum iaculis auctor sed magna. Quisque in urna sem. Nam cursus felis magna, vel tempor lectus ullamcorper congue. Integer mattis gravida dui eu maximus. Proin viverra mollis rutrum. Donec ut nisi nec erat vehicula maximus. Mauris eu nisl orci. Donec ac semper turpis, non egestas mi. Aenean volutpat, est quis tristique blandit, metus ipsum sodales turpis, ac lacinia velit nisi eu mauris. Vestibulum nec condimentum augue. Vestibulum nec ornare mauris, sed iaculis purus. Vivamus eu semper odio.',
     //  	  'department' => 'San Salvador',
     //  	  'country_id' => 1],
	    // ]);

	    // DB::table('institutions')->insert([
     //  	  [
     //  	  'id' => 5, 
     //  	  'name' => "Charity 5", 
     //  	  'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eleifend vehicula sodales. Nunc at sapien quam. Cras eu augue sit amet nulla fermentum iaculis auctor sed magna. Quisque in urna sem. Nam cursus felis magna, vel tempor lectus ullamcorper congue. Integer mattis gravida dui eu maximus. Proin viverra mollis rutrum. Donec ut nisi nec erat vehicula maximus. Mauris eu nisl orci. Donec ac semper turpis, non egestas mi. Aenean volutpat, est quis tristique blandit, metus ipsum sodales turpis, ac lacinia velit nisi eu mauris. Vestibulum nec condimentum augue. Vestibulum nec ornare mauris, sed iaculis purus. Vivamus eu semper odio.',
     //  	  'department' => 'San Miguel',
     //  	  'country_id' => 1],
	    // ]);

	    // DB::table('institutions')->insert([
     //  	  [
     //  	  'id' => 6, 
     //  	  'name' => "Charity 6", 
     //  	  'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eleifend vehicula sodales. Nunc at sapien quam. Cras eu augue sit amet nulla fermentum iaculis auctor sed magna. Quisque in urna sem. Nam cursus felis magna, vel tempor lectus ullamcorper congue. Integer mattis gravida dui eu maximus. Proin viverra mollis rutrum. Donec ut nisi nec erat vehicula maximus. Mauris eu nisl orci. Donec ac semper turpis, non egestas mi. Aenean volutpat, est quis tristique blandit, metus ipsum sodales turpis, ac lacinia velit nisi eu mauris. Vestibulum nec condimentum augue. Vestibulum nec ornare mauris, sed iaculis purus. Vivamus eu semper odio.',
     //  	  'department' => 'San Miguel',
     //  	  'country_id' => 1],
	    // ]);

	    // DB::table('institutions')->insert([
     //  	  [
     //  	  'id' => 7, 
     //  	  'name' => "Charity 7", 
     //  	  'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eleifend vehicula sodales. Nunc at sapien quam. Cras eu augue sit amet nulla fermentum iaculis auctor sed magna. Quisque in urna sem. Nam cursus felis magna, vel tempor lectus ullamcorper congue. Integer mattis gravida dui eu maximus. Proin viverra mollis rutrum. Donec ut nisi nec erat vehicula maximus. Mauris eu nisl orci. Donec ac semper turpis, non egestas mi. Aenean volutpat, est quis tristique blandit, metus ipsum sodales turpis, ac lacinia velit nisi eu mauris. Vestibulum nec condimentum augue. Vestibulum nec ornare mauris, sed iaculis purus. Vivamus eu semper odio.',
     //  	  'department' => 'San Miguel',
     //  	  'country_id' => 1],
	    // ]);
    }
}
