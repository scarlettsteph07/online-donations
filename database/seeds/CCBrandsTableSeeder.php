<?php

use Illuminate\Database\Seeder;

class CCBrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
      	  [
      	  'id' => 1, 
      	  'name' => "American Express", 
      	  'abbrv' => "AMEX"], 
	    ]);

	    DB::table('brands')->insert([
      	  [
      	  'id' => 2, 
      	  'name' => "MasterCard", 
      	  'abbrv' => "MC"], 
	    ]);

	    DB::table('brands')->insert([
      	  [
      	  'id' => 3, 
      	  'name' => "Visa International ", 
      	  'abbrv' => "Visa"], 
	    ]);

    }
}
