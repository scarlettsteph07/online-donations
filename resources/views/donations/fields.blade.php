<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
	<label class="col-md-4 control-label">Amount (US Dollar)</label>

	<div class="col-md-6">
    	<input type="number" min="1" step="any"  class="form-control" name="amount" value="{{ $donation->amount or old('amount') }}">

        @if ($errors->has('amount'))
            <span class="help-block">
                <strong>{{ $errors->first('amount') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('institution') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Intitution</label>

    <div class="col-md-6">

    	<select name="institution" class="form-control">
  		<option value="{{ $donation->institution->id or '' }}">{{ $donation->institution->name or 'Choose an option' }}</option>
  			@foreach ($institutions as $institution)
      		<option value="{{$institution->id}}">{{$institution->name}} - {{$institution->country->name}}</option>
    		@endforeach
  		</select>

        @if ($errors->has('institution'))
            <span class="help-block">
                <strong>{{ $errors->first('institution') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('card') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Credit Card</label>

    <div class="col-md-6">

    	<select name="card" class="form-control">
  		<option value="{{ $donation->card->id or '' }}">{{ $donation->card->name or 'Choose an option' }}</option>
  			@foreach ($cards as $card)
      		<option value="{{$card->id}}">{{$card->name . ' ending in '. $card->last_four}}</option>
    		@endforeach
  		</select>

        @if ($errors->has('card'))
            <span class="help-block">
                <strong>{{ $errors->first('card') }}</strong>
            </span>
        @endif
    </div>
</div>