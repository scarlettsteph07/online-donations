@extends('layouts.app')

@section('content')
@if(Auth::user() and Auth::user()->role_id==2)
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Your Donations</div>

                <div class="panel-body">
                    
                    <a class="btn btn-small btn-success pull-right" href="/users/{{$user->id}}/donations/create">
                    	<i class="fa fa-btn fa-money"></i> New Donation
                    </a>
	                <br>
                    <br>
                    <br>
                    <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td><i class="fa fa-calendar"></i> Date</td>
                            <td><i class="fa fa-building-o"></i> Institution</td>
                            <td><i class="fa fa-map-marker"></i> Location</td>
                            <td><i class="fa fa-money"></i> Amount</td>
                            <td><i class="fa fa-credit-card"></i> Credit Card</td>
                            <td><i class="fa fa-edit"></i> Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($donations as $donation)
                        <tr>
                            <td>{{ $donation->date }}</td>
                            <td>{{ $donation->institution->name }}</td>
                            <td>{{ $donation->institution->department}} - {{ $donation->institution->country->name }}</td> 
                            <td>{{ $donation->amount }}</td>
                            <td>{{ $donation->creditCard->name}} - {{ $donation->creditcard->last_four }}</td>              
                            <!-- add show, edit, and delete buttons -->
                            <td>
                
                                <!-- show the creditcard (uses the show method found at GET /donations/{id} -->
                                <a class="btn btn-small btn-info" href="/users/{{$user->id}}/donations/{{$donation->id}}"><i class="fa fa-info-circle"></i> Details</a>
                
                                <!-- edit this creditcard (uses the edit method found at GET /donations/{id}/edit -->
                                <!-- <a class="btn btn-small btn-warning" href="/users/{{$user->id}}/cards/{{$donation->id}}/edit"><i class="fa fa-pencil"></i> Edit</a> -->
                
                                <!-- delete the creditcard (uses the destroy method DESTROY /donations/{id} -->
                                <!-- <a class="btn btn-small btn-danger" href="/users/{{$user->id}}/cards/{{$donation->id}}/destroy"><i class="fa fa-minus-circle"></i> Delete</a> -->
                
                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pull-right">
                    {{ $donations->links() }}
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif(Auth::user() and Auth::user()->role_id==1)
    @include('errors.404')
@else
    @include('auth.login')
@endif
@endsection