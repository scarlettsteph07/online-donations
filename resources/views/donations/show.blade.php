@extends('layouts.app')

@section('content')
@if(Auth::user() and Auth::user()->role_id==2)
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                	Donation Detail
                	<div class="pull-right">
						        <a href="/users/{{$user->id}}/donations" class="form-group"><i class="fa fa-arrow-circle-left"></i> Back</i></a>
					        </div>
                </div>

                <div class="panel-body">
                    <div class="pull-left">
						<dl class="dl-horizontal">
  						<dt>Date:</dt>
  						<dd>{{ $donation->date }}</dd>
						</dl>
				
						<dl class="dl-horizontal">
  						<dt>Amount:</dt>
  						<dd>{{ $donation->amount }}</dd>
						</dl>

						<dl class="dl-horizontal">
  						<dt>Credit Card:</dt>
  						<dd>{{ $donation->creditCard->name }} - {{ $donation->creditCard->last_four }}</dd>
						</dl>
				
						<dl class="dl-horizontal">
  						<dt>Institution:</dt>
  						<dd>{{ $donation->institution->name }}</dd>
						</dl>	

						<dl class="dl-horizontal">
  						<dt>Destination Country:</dt>
  						<dd>{{ $donation->institution->country->name }}</dd>
						</dl>
					</div>
			
				<div class="pull-right">
					<!-- <a href="/" class="form-group btn btn-primary"><i class="fa fa-print"></i> Print Receipt</i></a> -->
				</div>

				</div>

                </div>
            </div>
        </div>
    </div>
</div>
@elseif(Auth::user() and Auth::user()->role_id==1)
    @include('errors.404')
@else
    @include('auth.login')
@endif
@endsection