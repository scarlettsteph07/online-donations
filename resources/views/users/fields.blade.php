<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	<label class="col-md-4 control-label">Name</label>

	<div class="col-md-6">
    	<input type="text" class="form-control" name="name" value="{{ $user->name or old('name') }}">

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Surname</label>

<div class="col-md-6">
    <input type="text" class="form-control" name="surname" value="{{ $user->surname or old('surname') }}">

        @if ($errors->has('surname'))
            <span class="help-block">
                <strong>{{ $errors->first('surname') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('document') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Document</label>

<div class="col-md-6">
    <input type="text" class="form-control" name="document" value="{{ $user->document or old('document') }}">

        @if ($errors->has('document'))
            <span class="help-block">
                <strong>{{ $errors->first('document') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">E-Mail Address</label>

    <div class="col-md-6">
        <input type="email" class="form-control" name="email" value="{{ $user->email or old('email') }}">

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>