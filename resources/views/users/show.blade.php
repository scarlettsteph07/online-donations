@extends('layouts.app')

@section('content')
@if(Auth::user() and Auth::user()->role_id==2)
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Profile
                </div>

                <div class="panel-body">
                    <div class="pull-left">
					<dl class="dl-horizontal">
  					<dt>Name:</dt>
  					<dd>{{ $user->name }}</dd>
					</dl>
			
					<dl class="dl-horizontal">
  					<dt>Surnames:</dt>
  					<dd>{{ $user->surname }}</dd>
					</dl>
			
					<dl class="dl-horizontal">
  					<dt>Document:</dt>
  					<dd>{{ $user->document }}</dd>
					</dl>	
					<dl class="dl-horizontal">
  					<dt>Email:</dt>
  					<dd>{{ $user->email }}</dd>
					</dl>
				</div>
			
				<div class="pull-right">
					<a href="{{ '/users/' . $user->id . '/edit' }}" class="form-group btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
          <br>
          <br>
          <a href="{{ '/users/' . $user->id . '/cards' }}" class="form-group btn btn-info"><i class="fa fa-credit-card"></i> Cards</a>

				</div>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif(Auth::user() and Auth::user()->role_id==1)
    @include('errors.404')
@else
    @include('auth.login')
@endif
@endsection