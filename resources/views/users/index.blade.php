@extends('layouts.app')

@section('content')
@if(Auth::user() and Auth::user()->role_id==1)
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">All Users</div>

                <div class="panel-body">
                    
                    <!-- <a class="btn btn-small btn-success pull-right" href="/admin/users/create">
                    	<i class="fa fa-btn fa-building-o"></i> New user
                    </a> -->
	                <br>
                    <br>
                    <br>
                    <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td><i class="fa fa-building-o"></i> Full Name</td>
                            <td><i class="fa fa-envelope-o"></i> Email Address</td>
                            <td><i class="fa fa-thumb-tack"></i> Rol</td>
                            <!-- <td><i class="fa fa-edit"></i> Actions</td> -->
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }} {{ $user->surname }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->role->name }}</td>
                
                            <!-- add show, edit, and delete buttons -->
                            <!-- <td> -->
                
                                <!-- show the creditcard (uses the show method found at GET /creditcards/{id} -->
                                <!-- <a class="btn btn-small btn-info" href="/admin/users/{{$user->id}}/"><i class="fa fa-info-circle"></i> Show</a> -->
                
                                <!-- edit this creditcard (uses the edit method found at GET /creditcards/{id}/edit -->
                                <!-- <a class="btn btn-small btn-warning" href="/admin/users/{{$user->id}}/edit"><i class="fa fa-pencil"></i> Edit</a> -->
                
                                <!-- delete the creditcard (uses the destroy method DESTROY /creditcards/{id} -->
                                <!-- <a class="btn btn-small btn-danger" href="/admin/users/{{$user->id}}/destroy"><i class="fa fa-minus-circle"></i> Delete</a> -->
                
                
                            <!-- </td> -->
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pull-right">
                    {{ $users->links() }}
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif(Auth::user() and Auth::user()->role_id==2)
    @include('errors.404')
@else
    @include('auth.login')
@endif
@endsection