@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    <h1 class="text-center">Visitors Counter</h1>
                    <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="/visitors" class="btn btn-small btn-default center-block">
                                    <i class="fa fa-btn fa-search-plus fa-5x"> {{$count}}</i>
                                </a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
