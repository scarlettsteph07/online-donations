@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                	All Visitors
                	<div class="pull-right">
                        <a href="/" class="form-group"><i class="fa fa-arrow-circle-left"></i> Back</i></a>
                    </div>
                </div>

                <div class="panel-body">
                    
                    <a class="btn btn-small btn-primary pull-right" href="/visitors/print">
                    	<i class="fa fa-btn fa-save"></i> Save .txt
                    </a>
	                <br>
                    <br>
                    <br>
                    <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td><i class="fa fa-calendar"></i> Date</td>
                            <td><i class="fa fa-search-plus"></i> Number of Visits</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($visitors as $visitor)
                        <tr>
                            <td>{{ $visitor->visit_date }}</td>
                            <td>{{ $visitor->total }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pull-right">
                    {{ $visitors->links() }}
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection