@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    404 Error
                    <div class="pull-right">
                        <a href="/" class="form-group"><i class="fa fa-home"></i> Home</i></a>
                    </div>
                </div>

                <div class="panel-body">
                    <h1 class="text-center"><i class="fa fa-search"></i> Page not found</h1>
                    <div class="form-group">
                            
                        </div>
                </div>
        </div>
    </div>
</div>
@endsection