@extends('layouts.app')

@section('content')
@if(Auth::user() and Auth::user()->role_id==1)
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">All Institutions</div>

                <div class="panel-body">
                    
                    <a class="btn btn-small btn-success pull-right" href="/admin/institutions/create">
                    	<i class="fa fa-btn fa-building-o"></i> New Institution
                    </a>
	                <br>
                    <br>
                    <br>
                    <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td><i class="fa fa-building-o"></i> Name</td>
                            <td><i class="fa fa-map-marker"></i> Department</td>
                            <td><i class="fa fa-globe"></i> Country</td>
                            <td><i class="fa fa-edit"></i> Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($institutions as $institution)
                        <tr>
                            <td>{{ $institution->name }}</td>
                            <td>{{ $institution->department }}</td>
                            <td>{{ $institution->country->name }}</td>
                
                            <!-- add show, edit, and delete buttons -->
                            <td>
                
                                <!-- show the creditcard (uses the show method found at GET /creditcards/{id} -->
                                <a class="btn btn-small btn-info" href="/admin/institutions/{{$institution->id}}/"><i class="fa fa-info-circle"></i> Details</a>
                
                                <!-- edit this creditcard (uses the edit method found at GET /creditcards/{id}/edit -->
                                <a class="btn btn-small btn-warning" href="/admin/institutions/{{$institution->id}}/edit"><i class="fa fa-pencil"></i> Edit</a>
                
                                <!-- delete the creditcard (uses the destroy method DESTROY /creditcards/{id} -->
                                <a class="btn btn-small btn-danger" href="/admin/institutions/{{$institution->id}}/destroy"><i class="fa fa-minus-circle"></i> Delete</a>
                
                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pull-right">
                    {!! $institutions->links() !!}
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
@elseif(Auth::user() and Auth::user()->role_id==2)
    @include('errors.404')
@else
    @include('auth.login')
@endif
@endsection