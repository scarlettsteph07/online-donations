<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	<label class="col-md-4 control-label">Name</label>

	<div class="col-md-6">
    	<input type="text" class="form-control" name="name" value="{{ $institution->name or old('name') }}">

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Description</label>

    <div class="col-md-6">
		<textarea name="description" class="form-control" rows="7" cols="50">{{ $institution->description or old('description') }}</textarea>
    	

        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Department</label>

<div class="col-md-6">
    <input type="text" class="form-control" name="department" value="{{ $institution->department or old('department') }}">

        @if ($errors->has('department'))
            <span class="help-block">
                <strong>{{ $errors->first('department') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Country</label>

    <div class="col-md-6">

    	<select name="country" class="form-control">
  		<option value="{{ $institution->country->id or '' }}">{{ $institution->country->name or 'Choose an option' }}</option>
  			@foreach ($countries as $country)
      		<option value="{{$country->id}}">{{$country->name}}</option>
    		@endforeach
  		</select>

        @if ($errors->has('country'))
            <span class="help-block">
                <strong>{{ $errors->first('country') }}</strong>
            </span>
        @endif
    </div>
</div>