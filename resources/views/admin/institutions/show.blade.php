@extends('layouts.app')

@section('content')
@if(Auth::user() and Auth::user()->role_id==1)
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Institution Information
                  <div class="pull-right">
                        <a href="/admin/institutions" class="form-group"><i class="fa fa-arrow-circle-left"></i> Back</i></a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="pull-left">
					<dl class="dl-horizontal">
  					<dt>Name:</dt>
  					<dd>{{ $institution->name }}</dd>
					</dl>
			
					<dl class="dl-horizontal">
  					<dt>Description:</dt>
  					<dd>{{ $institution->description }}</dd>
					</dl>
			
					<dl class="dl-horizontal">
  					<dt>Department:</dt>
  					<dd>{{ $institution->department }}</dd>
					</dl>	
					<dl class="dl-horizontal">
  					<dt>Country:</dt>
  					<dd>{{ $institution->country->name }}</dd>
					</dl>
				</div>
			
				<div class="pull-right">
					<a class="btn btn-small btn-warning center-block" href="/admin/institutions/{{$institution->id}}/edit"><i class="fa fa-pencil"></i> Edit</a>
				</div>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif(Auth::user() and Auth::user()->role_id==2)
    @include('errors.404')
@else
    @include('auth.login')
@endif
@endsection