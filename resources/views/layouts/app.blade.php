<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Donations</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                @if (Auth::guest())
                <!-- Branding Image -->
                <a class="navbar-brand" href="/">
                    Donations
                </a>
                @else
                <!-- <a href="/home"><i class="fa fa-btn fa-home"></i>Home</a> -->
                <a class="navbar-brand" href="#">
                    Donations
                </a>
                @endif
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <!-- <ul class="nav navbar-nav">
                    <li><a href="/home"><i class="fa fa-btn fa-home"></i>Home</a></li>
                </ul> -->

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="/login">Login</a></li>
                        <li><a href="/register">Register</a></li>
                    @else
                        <ul class="nav navbar-nav">
                            <li><a href="/home"><i class="fa fa-btn fa-home"></i>Home</a></li>
                        </ul>
                        @if (Auth::user()->role_id==1)
                            <li class="dropdown">
                            <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-btn fa-user"></i>{{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/users/{{ Auth::user()->id }}"><i class="fa fa-btn fa-user"></i>Profile</a></li>
                                <li><a href="/logout"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                
                            </ul> -->

                            <!-- <ul class="dropdown-menu" role="menu"> -->
                                <li><a href="/admin/users"><i class="fa fa-btn fa-user"></i>Users</a></li>
                                <li><a href="/admin/donations"><i class="fa fa-btn fa-money"></i>Donations</a></li>
                                <li><a href="/admin/institutions"><i class="fa fa-btn fa-building-o"></i>Institutions</a></li>
                                <li><a href="/admin/countries"><i class="fa fa-btn fa-globe"></i>Countries</a></li>
                                <li><a href="#"><i class="fa fa-btn fa-user"></i>Welcome, {{ Auth::user()->name }}</a></li>
                                <li><a href="/logout"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                
                            <!-- </ul> -->
                        </li>
                        @else
                        <li class="dropdown">
                            <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-btn fa-user"></i>{{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/users/{{ Auth::user()->id }}"><i class="fa fa-btn fa-user"></i>Profile</a></li>
                                <li><a href="/logout"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                
                            </ul> -->

                            <!-- <ul class="dropdown-menu" role="menu"> -->
                                <li><a href="/users/{{ Auth::user()->id }}"><i class="fa fa-btn fa-user"></i>Profile</a></li>
                                <li><a href="/users/{{ Auth::user()->id}}/cards"><i class="fa fa-btn fa-credit-card"></i>Cards</a></li>
                                <li><a href="/users/{{ Auth::user()->id}}/donations"><i class="fa fa-btn fa-money"></i>Donations</a></li>
                                <li><a href="#"><i class="fa fa-btn fa-user"></i>Welcome, {{ Auth::user()->name }}</a></li>
                                <li><a href="/logout"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                
                            <!-- </ul> -->
                        </li>
                        @endif
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
