<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	<label class="col-md-4 control-label">Name on Card</label>

	<div class="col-md-6">
    	<input type="text" class="form-control" name="name" value="{{ $card->name or old('name') }}">

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Card Number</label>

<div class="col-md-6">
    <input type="text" maxlength="16" pattern="[0-9]{13,16}" class="form-control" name="number" value="{{ $card->number or old('number') }}">

        @if ($errors->has('number'))
            <span class="help-block">
                <strong>{{ $errors->first('number') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Last Four</label>
	<div class="col-md-6">
    	<input type="text" maxlength="4" pattern="[0-9]{4}" class="form-control" name="last_four" value="{{ $card->last_four or old('last_four') }}">

        @if ($errors->has('last_four'))
            <span class="help-block">
                <strong>{{ $errors->first('last_four') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('expiration') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Expiration</label>

<div class="col-md-6">
    <input type="text" class="form-control" name="expiration" value="{{ $card->expiration or old('expiration') }}">

        @if ($errors->has('expiration'))
            <span class="help-block">
                <strong>{{ $errors->first('expiration') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('cvv') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">CVV Number</label>

    <div class="col-md-6">
        <input type="text" maxlength="3" pattern="[0-9]{3}" class="form-control" name="cvv" value="{{ $card->cvv or old('cvv') }}">

        @if ($errors->has('cvv'))
            <span class="help-block">
                <strong>{{ $errors->first('cvv') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('brand') ? ' has-error' : '' }}">
    <label class="col-md-4 control-label">Brand (Issuer)</label>

    <div class="col-md-6">

    	<select name="brand" class="form-control">
  		<option value="{{ $card->brand->id or '' }}">{{ $card->brand->name or 'Choose an option' }}</option>
  			@foreach ($brands as $brand)
      		<option value="{{$brand->id}}">{{$brand->name}}</option>
    		@endforeach
  		</select>

        @if ($errors->has('brand'))
            <span class="help-block">
                <strong>{{ $errors->first('brand') }}</strong>
            </span>
        @endif
    </div>
</div>