@extends('layouts.app')

@section('content')
@if(Auth::user() and Auth::user()->role_id==2)
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Card Ending {{$card->last_four}}
                    <div class="pull-right">
                        <a href="/users/{{$user->id}}/cards" class="form-group"><i class="fa fa-arrow-circle-left"></i> Back</i></a>
                    </div>
                </div>

                <div class="panel-body">
                    
                    <form class="form-horizontal" role="form" method="POST" action="/users/{{$user->id}}/cards/{{$card->id}}/update">
                    	{!! csrf_field() !!}
                    	
                    	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label class="col-md-4 control-label">Name on Card</label>
						
							<div class="col-md-6">
    							<input type="text" class="form-control" name="name" value="{{ $card->name or old('name') }}">
						
        						@if ($errors->has('name'))
            						<span class="help-block">
                						<strong>{{ $errors->first('name') }}</strong>
            						</span>
        						@endif
    						</div>
						</div>

						<div class="form-group{{ $errors->has('expiration') ? ' has-error' : '' }}">
    						<label class="col-md-4 control-label">Expiration</label>
						
						<div class="col-md-6">
    						<input type="text" class="form-control" name="expiration" value="{{ $card->expiration or old('expiration') }}">
						
        						@if ($errors->has('expiration'))
            						<span class="help-block">
                						<strong>{{ $errors->first('expiration') }}</strong>
            						</span>
        						@endif
    						</div>
						</div>
						
						<div class="form-group{{ $errors->has('cvv') ? ' has-error' : '' }}">
    						<label class="col-md-4 control-label">CVV Number</label>
						
    						<div class="col-md-6">
        						<input type="text" maxlength="3" pattern="[0-9]{3}" class="form-control" name="cvv" value="{{ $card->cvv or old('cvv') }}">
						
        						@if ($errors->has('cvv'))
            						<span class="help-block">
                						<strong>{{ $errors->first('cvv') }}</strong>
            						</span>
        						@endif
    						</div>
						</div>


                        <div class="form-group">
                        	<div class="col-md-6 col-md-offset-4">
                            	<button type="submit" class="btn btn-warning center-block">
                                	<i class="fa fa-btn fa-money"></i>Update Credit Card
                            	</button>
                        	</div>
                    	</div>
                    </form>

                    
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif(Auth::user() and Auth::user()->role_id==1)
    @include('errors.404')
@else
    @include('auth.login')
@endif
@endsection