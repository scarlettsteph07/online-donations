@extends('layouts.app')

@section('content')
@if(Auth::user() and Auth::user()->role_id==2)
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add New Credit Card 
                    <div class="pull-right">
                        <a href="/users/{{$user->id}}/cards" class="form-group"><i class="fa fa-arrow-circle-left"></i> Back</i></a>
                    </div>
                </div>

                <div class="panel-body">
                    
                    <form class="form-horizontal" role="form" method="POST" action="/users/{{$user->id}}/cards">
                    	{!! csrf_field() !!}
                    	@include('creditcards.fields')

                        <div class="form-group">
                        	<div class="col-md-6 col-md-offset-4">
                            	<button type="submit" class="btn btn-success center-block">
                                	<i class="fa fa-btn fa-money"></i>Add Credit Card
                            	</button>
                        	</div>
                    	</div>
                    </form>

                    
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif(Auth::user() and Auth::user()->role_id==1)
    @include('errors.404')
@else
    @include('auth.login')
@endif
@endsection