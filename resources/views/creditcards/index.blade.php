@extends('layouts.app')

@section('content')
@if(Auth::user() and Auth::user()->role_id==2)
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Credit Cards Information</div>

                <div class="panel-body">
                    
                    <a class="btn btn-small btn-success pull-right" href="/users/{{$user->id}}/cards/create">
                    	<i class="fa fa-btn fa-credit-card"></i> New Card
                    </a>
	                <br>
                    <br>
                    <br>
                    <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <td><i class="fa fa-tag"></i> Name</td>
                            <td><i class="fa fa-credit-card"></i> Last Four</td>
                            <td><i class="fa fa-calendar"></i> Expiration</td>
                            <!-- <td><i class="fa fa-building-o"></i> Brand</td> -->
                            <td><i class="fa fa-edit"></i> Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($cards as $card)
                        <tr>
                            <td>{{ $card->name }}</td>
                            <td>{{ $card->last_four }}</td>
                            <td>{{ $card->expiration }}</td>
                            <!-- <td>{{ $card->brand}}</td> -->
                
                            <!-- add show, edit, and delete buttons -->
                            <td>
                
                                <!-- show the creditcard (uses the show method found at GET /creditcards/{id} -->
                                <!-- <a class="btn btn-small btn-success" href="{{ '/creditcards/' . $card->id }}"><i class="fa fa-money"></i> Show</a> -->
                
                                <!-- edit this creditcard (uses the edit method found at GET /creditcards/{id}/edit -->
                                <a class="btn btn-small btn-warning" href="/users/{{$user->id}}/cards/{{$card->id}}/edit"><i class="fa fa-pencil"></i> Edit</a>
                
                                <!-- delete the creditcard (uses the destroy method DESTROY /creditcards/{id} -->
                                <a class="btn btn-small btn-danger" href="/users/{{$user->id}}/cards/{{$card->id}}/destroy"><i class="fa fa-minus-circle"></i> Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pull-right">
                    {{ $cards->links() }}
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif(Auth::user() and Auth::user()->role_id==1)
    @include('errors.404')
@else
    @include('auth.login')
@endif
@endsection
